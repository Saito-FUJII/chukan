\documentclass[twocolumn,a4paper]{chukan}
%\usepackage{epsfig}
\usepackage[dvipdfmx]{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{algorithmic}
\usepackage{color,listings,lstcoq}%listing用
\usepackage{url}

\definecolor{ltblue}{rgb}{0,0.4,0.4}
\definecolor{dkblue}{rgb}{0,0.1,0.6}
\definecolor{dkgreen}{rgb}{0,0.5,0}
\definecolor{dkviolet}{rgb}{1.0,0,0}
\definecolor{dkred}{rgb}{0.5,0,0}

\newcommand{\Emph}[1]{\textbf{#1}}
\lstset{basicstyle= {\ttfamily}, escapechar=\@, numbers= none, xleftmargin= 5pt} %lisntingsの設定


\title{11. 証明支援系 Coq を用いた有界モデル検査アルゴリズムの検証}
\author{17740193 \qquad 藤井 采人}

\begin{document}
\maketitle

\section{はじめに}
有界モデル検査 (\ref{s:BMC}章)は, システムの検証問題を述語論理式にエンコードし, 
高性能なSMT ソルバーで解くことにより, システムの安全性を示したり, バグを見つけたりするのに有用な方法である. 
さまざまな有界モデル検査を行うためのアルゴリズム(エンコード法)が提案されているが, 
それぞれの方法は, 帰納法や状態の抽象表現, 実行パスの unrolling など煩雑な考察に基づいて設計されている\cite{IC3, k-Induction}.
そのため, エンコード法の実装や改変を行う際には, 背景の考察を理解して行わねばならず, 困難を伴う.

%本研究では, ユーザがエンコード法を簡潔に定義し, プラグインされたSMTソルバーを使って検査を実施したり, 
%エンコード法と検査法の正しさを証明したりすることを可能にするための環境を証明支援系 Coq (\ref{s:Coq}章)上に構築することを目指す. 
本研究では, ユーザーがエンコード法を簡潔に記述し, 
プラグインされたSMTソルバーを使い, 対話的に有界モデル検査を実施できる環境を証明支援系Coq上に構築した.
さらに, 本環境下では, 検査の実施だけでなくエンコード法と検査法自体の健全性を示すことで,
高信頼な検査を実施することも可能である.
本稿では, 構築した環境についてその概要を述べる.


\section{関連研究}
さまざまなクラスのシステムや性質を対象としたさまざまな有界モデル検査ツールが開発されている. 
たとえば NuSMV\footnote{\url{http://nusmv.fbk.eu}}は有限状態遷移系とLTLで記述した性質を対象とした有界モデル検査機能を備える. 
こうしたツールの問題点として, ツール自体の正しさが保証されていない点や, 検査結果の確認が容易でない点が指摘されている\cite{MT2016}. 
また, ツールにユーザが新たなエンコード方法を追加しようとする場合, ツールの実装を理解し, 適切に追加実装を行う必要がある. 
こうした実装作業はユーザに多大な負担を強いるうえ, 追加実装が正しく行われたかどうかを確認するのも容易ではない.

\section{証明支援系 Coq と SMTソルバー}
\label{s:Coq}
\Emph{Coq} はフランスのINRIAで開発されている定理証明支援系である. 
Coq は, 高階述語論理式, 型, 関数 などを記述するための言語を備え, 証明したい定理を記述することができる. 
記述した定理について, 対話環境上でタクティックと呼ばれる専用コマンド群を用い, 証明を記述することができる.

\Emph{SMTソルバー} (Satisfiability Modulo Theories solver)とは, 述語論理式の充足性を判定できる自動定理証明器である. 
例えば, 
%Alt-Ergo\footnote{\url{http://alt-ergo.lri.fr/}}, 
Z3\footnote{\url{https://github.com/Z3Prover/z3}}, 
CVC4\footnote{\url{http://cvc4.cs.nyu.edu/web/}}などがある. 
%述語論理とは, 命題論理を拡張したものであり, 量化子 (∀, ∃), 整数・実数などの変数, 関数, 述語などを扱うことができる.

\section{有界モデル検査}
\label{s:BMC}
%モデル検査は, システム(有限状態遷移系)と満たすべき性質を入力として与え, 
%性質に違反するシステムの実行パス (反例)を網羅的に探索し, システムが性質を満たしているか検証する手法である.
%
%モデル検査は, システム(有限状態遷移系)と満たすべき性質を入力として与え, 
%システムが性質を満たしているか検証する手法である.
%原理的には自動的に解くことが可能であるが, 状態空間が巨大になった場合, 探索に膨大な時間を要する. 
%そこで,
モデル検査の探索範囲を一定の深さ$k$に限定し, システムの不具合を効率良く検出する\Emph{有界モデル検査}が提案されている\cite{BMC}. 
基本的な考え方は, ステップ数$k$以下のシステムの振る舞いと性質を述語論理式で記述して検査を行う手法である. 
具体的には, システム (初期状態の制約$I$, 遷移関係の制約$T$), 性質$P$, 自然数$k$を入力とし, 安全性の必要条件や十分条件を述語論理式にエンコードし, 
その述語論理式の成否をSMTソルバーによって判定し, 検査を行う手法である.  
以下, 本節ではその概要を説明する.

まず, 
$s$, $s′$ をシステムの状態としたとき, 述語論理式$I(s)$, $T(s,s′)$, $P(s)$ によりそれぞれ, $s$が初期状態であること, 
$s$と$s′$が遷移関係にあること, $s$が$P$を満たすことをエンコードできるものとする. 
%
つぎに, 状態列$s_0, s_1, .., s_k$を$s_{[0..k]}$と表せるものとすると, 
状態$s_0$から$k$ステップ目の状態$s_k$までの実行パスであることを
式$\mathit{path}(s_{[0..k]})$としてエンコードできる:
\begin{align} %path
 \label{eq:path}
    \mathit{path}(s_{[0..k]})  :=  \bigwedge_{0 \leq i < k}T(s_i,s_{i+1})
\end{align} 
%
%さらに, システムの$k$ステップ以内の状態が$P$を満たさないことを
%以下のようにエンコードできる:
%\begin{align} %Biere
% \label{eq:Biere}
%  I(s_0) \ \wedge \ path(s_{[0..k]}) \ \wedge  \bigvee_{0 \leq i \leq k} \neg P(s_i) 
%\end{align} 
%%
%SAT/SMTソルバーにより条件
%\begin{align} %Biere
%    \label{eq:Biere:post}
%  \neg\exists s_{[0..k]}. \ \text{(式\eqref{eq:Biere})}
%\end{align} 
%
SMTソルバーにより条件
\begin{align} %Biere
    \label{eq:Biere:post}
  \neg\exists s_{[0..k]}. \ I(s_0) \ \wedge \ path(s_{[0..k]}) \ \wedge  \bigvee_{0 \leq i \leq k} \neg P(s_i) 
\end{align} 
が成り立つことを示せれば,
システムが$k$ステップまでは安全であることが分かる.
この方法では, $k$ステップより後の状態を含むすべての状態については安全であることは言えない. 

Sheeranらは, 帰納法に基づき簡潔な式にエンコードする6つの方法を提案している\cite{k-Induction}.
ここでは, 方法1による安全性の検査について述べる.
まず, システムが$k$ステップ以内で安全であることを
\begin{align} %kth_P_safe
    \label{eq:kth-P-Safe}
    \bigwedge_{0 \leq i \leq k} \bigl(\neg\exists s_{[0..i]}. \ I(s_0) \ \land \ \mathit{path}(s_{[0..i]})\ \land \ \neg P(s_i)\bigr) 
\end{align} 
とエンコードすることができ, SMTソルバーで各$i$について部分式が充足不能であることを確認できれば, 示すことができる.

つぎに,
実行パス$s_{[0..k]}$に状態の重複がない (閉路がない) ことを式$\mathit{loopFree}(s_{[0..k]})$としてエンコードする:
\begin{align} %loopFree
 \label{eq:loopfree}
    \mathit{loopFree}(s_{[0..k]}) :=  \mathit{path}(s_{[0..k]}) \ \land
   \bigwedge_{0 \leq i <  j < k} s_i \neq s_j 
\end{align} 
%
以下の条件を考える:
\begin{multline}%Sheeran1
    \Bigl(~(\neg \exists s_{[0..k]}. \  I(s_0) \land \mathit{loopFree}(s_{[0..k]})) \ \lor \\
    (\neg\exists s_{[0..k]}. \ \mathit{loopFree}(s_{[0..k]})  \ \land \ \neg P(s_k))~\Bigr) ~~ \\ %\nonumber \\
    \land \bigwedge_{0 \leq i \leq k} \bigl(\neg\exists s_{[0..i]}. \ I(s_0)  \land  \mathit{path}(s_{[0..i]}) \land  \neg P(s_i)\bigr) 
    \label{eq:Sheeran1} %Sheeran
\end{multline}
上記が成り立つとき, すべての状態についてシステムが安全であることが言える.


%\section{Sheeranらの方法}
%Sheeranらは, 帰納法を利用することで, $k$ステップ目以降の安全性を示す方法を6つ提案している\cite{k-Induction}.
%本節では, 通常の帰納法を拡張した$k$-Inductionを利用した検査法 (Sheeranらの方法2)について説明する. 
%
%\subsection{$k$-Induction}
%$k$-Inductionとは, 通常の帰納法を$k$まで拡張したものである.
%具体的には, 自然数を引数とする命題関数$X$について, 
%ある$k\geq1$の$k$で式\eqref{eq:k-BC}と式\eqref{eq:k-IC}が成り立つとき, すべての整数$n$について$X(n)$が成り立つというというものである:
%
%\begin{align} %Biere
  %\bigwedge_{0 \leq i < k}X(i) \label{eq:k-BC}\\ 
%  \forall n. \Bigl (~\bigwedge_{0 \leq i < k} X(i+n) \to X(n + k) ~\Bigr ) \label{eq:k-IC}
%\end{align} 

%\subsection{Sheeranらの方法2}
%以下, Sheeranらの方法2について説明する.
%まず, システムが$k$ステップ以内で安全であることを
%\begin{align} %loopFree
%    \label{eq:kth-P-Safe}
%    \bigwedge_{0 \leq i \leq k} \bigl(\neg\exists s_{[0..i]}. \ I(s_0) \ \land \ \mathit{path}(s_{[0..i]})\ \land \ \neg P(s_i)\bigr) 
%\end{align} 
%とエンコードすることができ, SAT/SMTソルバーで各$i$について部分式が充足不能であることを確認できれば, 示すことができる.
%
%つぎに, 実行パス$s_{[0..k]}$に状態の重複がない (閉路がない) ことを式$\mathit{loopFree}(s_{[0..k]})$としてエンコードする:
%\begin{align} %loopFree
% \label{eq:loopfree}
%    \mathit{loopFree}(s_{[0..k]})  :=  \mathit{path}(s_{[0..k]}) \ \wedge 
%%\end{align} 
%
%以下の条件を考える:
%\begin{multline}
%  \Bigl(~(\neg \exists s_{[0..k]}. \  I(s_0) \  \land \ \mathit{loopFree}(s_{[0..k]})) \ \lor \\
 % (\neg\exists s_{[0..k]}. \ \mathit{loopFree}(s_{[0..k]})) \ \land 
%    \bigwedge_{0 \leq i < k} P(s_i) \ \land \ \neg P(s_k))~\Bigr)  \\
%    \land \ \text{(式\eqref{eq:kth-P-Safe})}
%    \label{eq:Sheeran1} %Sheeran2
%\end{multline}
%上記が成り立つとき, すべての状態についてシステムが安全であることが言える.
%

\section{Coq上での有界モデル検査}
\label{s:coq-bmc}
本章では, Coq上で有界モデル検査を行う方法について述べる.

\subsection{Sheeranらの方法1}
\label{s:coq-bmc:Sheeran1}
本環境下では, 式\eqref{eq:Sheeran1}を\texttt{Sheeran\_method1}として以下のように記述することができる:
%
%% Sheeran_method1
\begin{lstlisting}[language = Coq, frame=tb, framesep=5pt, breaklines = true] 
Definition Sheeran_method1 (I : init) (T : trans) 
  (P : property) (l : list nat) (k : nat) : Prop :=
  ((forall s1 : ss, lasso I T s1 0 k l) \/
    forall s2 : ss, violate_loop_free T P s2 0 k l)) 
  /\ kth_P_safe I T P k.
\end{lstlisting}
%
引数\texttt{I}, \texttt{T}, \texttt{P}, \texttt{k}は, 
初期状態の制約$I$, 遷移関係の制約$T$, 性質$P$, 探索ステップ数$k$を表しており,
自然数のリスト\texttt{l}は, システムの状態を扱う際に必要となるものである.
エンコードのために記述した関数\texttt{lasso}, \texttt{violate\_loop\_free}, \texttt{kth\_P\_safe}, 
状態列を表す型\texttt{ss}, システムを記述すための型\texttt{init}, \texttt{trans}, 性質を記述すための型\texttt{property}の定義は省略する.
詳細は, \cite{BMC-Coq}を参照されたい.


\subsection{健全性の証明}
\label{s:coq-bmc:soundness}
本環境では, ユーザーがエンコード法と検査法の健全性を示すことができる.
ここでは, Sheeranらの方法１の健全性を示す.

まず, システム安全であるとき, 以下の条件式が成り立つことがわかっている:
\begin{align}
 \label{eq:safety3} %
  \forall i. \ \forall s_{[0...i]}. \ \neg (I(s_0)  \wedge \mathit{loopFree}(s_{[0..k]}) \ \wedge \neg P(s_i))
\end{align}
従って,「式\eqref{eq:Sheeran1}が成り立つならば, 式\eqref{eq:safety3}が成り立つ」を証明すれば, 
Sheeranらの方法1の健全性を示せたことになる.


Coq上で, \texttt{Sheeran\_method1}の健全性を示す場合は, 定理\texttt{Proof\_Sheeran\_method1}として以下のように記述する:
%
%% Proof_Sheeran_method1
\begin{lstlisting}[language = Coq, frame=tb, framesep=5pt, breaklines = true] 
Theorem Proof_Sheeran_method1 : forall (k : nat) (I : init) 
(T : trans) (P : property) (l : list nat),
  Sheeran_method1 I T P l k
  -> (forall (i : nat) (s : ss),
     ~ (I (s 0) /\ loop_free T s 0 i l /\ ~ P (s i))).
\end{lstlisting}
そして, 上記の定理に対して, ユーザーが証明コードを別途記述することで, 証明を行うことが出来る.
本研究では, 16個の補題と250行ほどの証明コードにより, 定理\texttt{Proof\_Sheeran\_method1}の証明に成功している.
%特に, 閉路を持たない実行パス$\mathit{loopFree}(s_{[0..(i+j)]}$は,
%
%\begin{multline} %loopFree
% \label{eq:divide-loopfree}
%    \mathit{loopFree}(s_{[0..(i+j)]}) \to \\
%    \mathit{loopFree}(s_{[0..i]}) \land \mathit{loopFree}(s_{[i..(i+j)]})
%\end{multline} 
%という関係が成り立つという補題\texttt{divide\_loop\_free}の証明に, 
%150行ほどの証明コードを使用している.
ただし現状, 未証明な補題が1つ課題として残っている.
証明の詳細については, 本稿では省略する\cite{BMC-Coq}.

\subsection{検査の実施}
本節では, \ref{s:coq-bmc:Sheeran1}節で定義した\texttt{Sheeran\_method1}によって検査を行う方法を示す.
まず, 検査するシステム ($I$, $T$, 自然数のリスト$l$), 性質$P$, 探索範囲$k$を入力として, 
以下のように記述する:
%
%% test
\begin{lstlisting}[language = Coq, frame=tb, framesep=5pt, breaklines = true] 
Theorem test : Sheeran_method1  @$I$ $T$ $P$ $l$ $k$@.
Proof.
 unfold I; unfold T; unfold P.
 unfold ... 
 @\color[rgb]{0.5,0,0}(*$I$, $T$, $P$内に定義された関数をすべて展開*)@
 ...
 simpl.
 Sheeran_smt_solve1.
Qed.
\end{lstlisting}
%
そして, \texttt{unfold}タクティックによって, $I$, $T$, $P$内に定義された関数をすべて展開し,
\texttt{simpl}タクティックによって, 展開された式の簡約を行う.
その後, \texttt{Sheeran\_method1}の各部分式をSMTソルバーによって評価するタクティック\texttt{Sheeran\_smt\_solve1}により証明を試みる\footnote{SMTソルバーの呼び出しには, Coq-smt-checkを使用している. \\ \qquad\url{https://github.com/gmalecha/coq-smt-check}}.
検査が成功した場合は, Coq上にその結果を反映し,
失敗した場合はエラーとなる実行パスをCoq上に表示する.
本稿では, $I$, $T$, $P$, $l$の記述方法については省略する\cite{BMC-Coq}.


\section{まとめ}
本稿では, Coq上にエンコード法を簡潔に記述でき, ユーザーがエンコード法と検査法の健全性を示すことにより,
高信頼な有界モデル検査を実施できる環境について述べた.
本研究では, 文献\cite{k-Induction}で提案されている, 方法1, 方法2 について, 検査を行う関数を記述し, その健全性の証明も行った.
また, 方法2は$k$-Inductionとして知られており, さまざまなアルゴリズムに利用されているため, 
より広範囲な応用が期待される.

今後は, 方法1, 方法2の健全性の証明のなかで,未証明となっている補題を証明し, 
健全性の証明をより完全なものにする.





\begin{thebibliography}{9}

\bibitem{MT2016}
        A. Mebsout and C. Tinelli:
        Proof Certificates for SMT-based Model Checkers for Infinite-state Systems.
        FMCAD,
        pp.~117--124 (2016)

\bibitem{BMC}
        A. Biere, A. Cimatti, E. M. Clarke, and Y. Zhu: 
        Symbolic Model Checking without BDDs.
        TACAS, 
        LNCS~1579, pp.~193--207 (1999).
  
\bibitem{IC3}
        A. R. Bradley : 
        SAT-based model checking without unrolling.
        VMCAI, LNCS~6538,
        pp.~1--14 (2011).

%\bibitem{ITP} 
%        K. L. McMillan:
%        Interpolation and SAT-based model checking.
%        CAV,
%        LNCS~2725, pp.~1--13 (2003).

\bibitem{BMC-Coq}
	\url{https://bitbucket.org/Saito-FUJII/bmc/}
	
\bibitem{k-Induction} 
	M. Sheeran, S. Singh, and G. St\r{a}lmarck: 
        Checking safety properties using induction and a SAT-solver.
        FMCAD, LNCS~1954, pp.~108--125 (2000). 
\end{thebibliography}

\end{document}
